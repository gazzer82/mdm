	curl -fs https://www.blackmagicdesign.com/api/support/us/downloads.json | /usr/bin/osascript -l 'JavaScript' \
		-e "let json = $.NSString.alloc.initWithDataEncoding($.NSFileHandle.fileHandleWithStandardInput.readDataToEndOfFile$(/usr/bin/uname -r | /usr/bin/awk -F '.' '($1 > 18) { print "AndReturnError(ObjC.wrap())" }'), $.NSUTF8StringEncoding)" \
		-e 'if ($.NSFileManager.defaultManager.fileExistsAtPath(json)) json = $.NSString.stringWithContentsOfFileEncodingError(json, $.NSUTF8StringEncoding, ObjC.wrap())' \
		-e 'parsed = JSON.parse(json.js)' \
    -e "converters = parsed.downloads.filter((download) => download.name.match(/^Blackmagic Converters/))[0]" \
    -e "converters.name.match(/(\d+\.\d+)/)[0]"