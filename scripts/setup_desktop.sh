#!/bin/sh
# Set Desktop background

# After the Apple Setup completed. Now safe to grab the current user and user ID
CURRENT_USER=$(/usr/bin/stat -f "%Su" /dev/console)

desktoppr="/usr/local/bin/desktoppr"

mkdir -p '/Library/Desktop Pictures'

curl -s --fail-with-body 'https://gitlab.com/gazzer82/mdm/-/raw/main/images/1920_1080_Rental_Laptop.png?ref_type=heads&inline=false' -o '/Library/Desktop Pictures/desktop.png'

sudo -u "$CURRENT_USER" ${desktoppr} "/Library/Desktop Pictures/desktop.png"