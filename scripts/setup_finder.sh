#!/bin/sh
# Show the path bar in Finder

CURRENT_USER=$(/usr/bin/stat -f "%Su" /dev/console)

sudo -u "$CURRENT_USER" /usr/bin/defaults write com.apple.finder ShowPathbar -bool true

sudo -u "$CURRENT_USER" killall Finder