#!/bin/sh

####################################################################################################
#
# LOGGING FUNCTION
#
####################################################################################################

log () {
	echo $1
	echo $(date "+%Y-%m-%d %H:%M:%S: ") $1 >> $logFile
}

#Download the DMG
log "Downloading the git installer"
curl -L  https://sourceforge.net/projects/git-osx-installer/files/git-2.33.0-intel-universal-mavericks.dmg/download > /usr/local/Baseline/Packages/git.dmg


# Mount the DMG
log "Mounting the DMG $dmgName..."
mountResult=`/usr/bin/hdiutil mount -private -noautoopen -noverify /usr/local/Baseline/Packages/git.dmg -shadow -mountpoint /Volumes/git`
mountVolume=`echo "$mountResult" | grep Volumes | awk '{print $3}'`
mountDevice=`echo "$mountResult" | grep disk | head -1 | awk '{print $1}'`

if [ $? == 0 ]; then
	log " DMG mounted successfully as volume $mountVolume on device $mountDevice."
else
	log "There was an error mounting the DMG. Exit Code: $?"
fi

# Find the PKG in the DMG
packageName=`ls $mountVolume | grep "pkg"`

# log "Copying Package $packageName from mount path $mountVolume... to Baseline packages folder"
# # Copy the package to the INstall directory



# Install the PKG wrapped inside the DMG
log "Installing Package $packageName from mount path $mountVolume..."

installer -pkg "$mountVolume/$packageName" -target /
# /usr/local/jamf/bin/jamf install -path $mountVolume -package "$packageName"

# if [ $? == 0 ]; then
# 	log "Package successfully installed."
# else
# 	log "There was an error installing the package. Exit Code: $?"
# fi

# Unmount the DMG
echo "Unmounting disk $mountDevice..."
hdiutil detach "$mountDevice" -force

# Delete the DMG
/bin/rm /usr/local/Baseline/Packages/git.dmg