!/bin/zsh

##### Dockutil initial setup
export PATH=/usr/bin:/bin:/usr/sbin:/sbin

# COLLECT IMPORTANT USER INFORMATION
# Get the currently logged in user
currentUser=up

# Get uid logged in user
uid=$(id -u "${currentUser}")

# Current User home folder - do it this way in case the folder isn't in /Users
userHome=$(dscl . -read /users/${currentUser} NFSHomeDirectory | cut -d " " -f 2)

# Path to plist
plist="${userHome}/Library/Preferences/com.apple.dock.plist"

# Convenience function to run a command as the current user
# usage: runAsUser command arguments...


# Convenience function to run a command as the current user
# usage: runAsUser command arguments...
runAsUser() {  
	if [[ "${currentUser}" != "loginwindow" ]]; then
		launchctl asuser "$uid" sudo -u "${currentUser}" "$@"
	else
		echo "no user logged in"
		exit 1
	fi
}

echo "------------------------------------------------------------------------"
echo "Running as logged-in user: $currentUser"
echo "------------------------------------------------------------------------"

echo "Setting dock to Auto Hide"

runAsUser /usr/bin/defaults write ${plist} autohide -bool true

sleep 2

echo "Removing all from Dock"

runAsUser /usr/local/bin/dockutil --remove all --no-restart ${plist}

sleep 5

echo "Adding applications to Dock"

runAsUser /usr/local/bin/dockutil --add /Applications/Google\ Chrome.app --no-restart ${plist}
runAsUser /usr/local/bin/dockutil --add /Applications/Safari.app --no-restart ${plist}
runAsUser /usr/local/bin/dockutil --add /System/Applications/System\ Settings.app --no-restart ${plist}

sleep 2

echo "Resetting Dock"

killall -KILL Dock


