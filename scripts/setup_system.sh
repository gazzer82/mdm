#!/bin/bash
# Setup power options

# Sets Turn Display Off after... Never

systemsetup -setsleep never

# Sets Prevent computer from sleeping automatically

systemsetup -setcomputersleep never

# Sets Put Hard Disks to Sleep when possible off

pmset -c disksleep 0

# Sets Wake for network acccess to on

systemsetup -setwakeonnetworkaccess on

# sets Startup automatically after a power failure

systemsetup -setrestartpowerfailure on

# turns off Power Nap in Energy Saver

/usr/bin/pmset -c darkwakes 0
/usr/bin/pmset pmset -a sleep 0
/usr/bin/pmset pmset -a displaysleep 0
/usr/bin/pmset pmset -a disksleep 0

# disable spotlight

/usr/bin/mdutil -ai off

# disable Time Machine

/usr/bin/tmutil disable

# Disable separate Spaces

/usr/bin/defaults write com.apple.spaces spans-displays -bool TRUE


exit 0