#!/bin/bash
# Install Sofwtare via AutoPKG

# COLLECT IMPORTANT USER INFORMATION
# Get the currently logged in user
currentUser=$( echo "show State:/Users/ConsoleUser" | scutil | awk '/Name :/ { print $3 }' )
AUTOPKG="/usr/local/bin/autopkg"

# global check if there is a user logged in
if [ -z "$currentUser" -o "$currentUser" = "loginwindow" ]; then
  echo "no user logged in, cannot proceed"
  exit 1
fi
# now we know a user is logged in

# get the current user's UID
uid=$(id -u "$currentUser")
echo "running as user "${currentUser}""

# Convenience function to run a command as the current user
# usage: runAsUser command arguments...
runAsUser() {  
	if [[ "${currentUser}" != "loginwindow" ]]; then
		launchctl asuser "$uid" sudo -u "${currentUser}" "$@"
	else
		echo "no user logged in"
		exit 1
  logged in
    echo "At login window"
    exit 1
	fi
}

#install AutoPKG

AUTOPKG_LATEST="https://github.com/autopkg/autopkg/releases/download/v2.7.2/autopkg-2.7.2.pkg"

runAsUser /usr/bin/curl -L "${AUTOPKG_LATEST}" -o "/users/${currentUser}/autopkg-latest.pkg"

installer -pkg "/users/${currentUser}/autopkg-latest.pkg" -target /

echo "### AutoPkg Installed"

# Clean Up When Done
runAsUser rm "/users/${currentUser}/autopkg-latest.pkg"

#Add the autopkg repo

cd /Users/Shared

echo "Adding the AutoPKG Repo"
runAsUser ${AUTOPKG} repo-add https://gitlab.com/gazzer82/mdm

#Add the autopkg repo

echo "Installing Blackmagic Converters"
runAsUser ${AUTOPKG} run -v BlackmagicConverters.download

echo "Installing Blackmagic Videohub"
runAsUser ${AUTOPKG} run -v BlackMagicVideohub.download

echo "Installing Blackmagic Converters"
runAsUser ${AUTOPKG} run -v BlackMagicVideohub.download

echo "Installing QLab 5"
runAsUser ${AUTOPKG} run -v Qlab5.download

echo "Installing Resolume Arena"
runAsUser ${AUTOPKG} run -v ResolumeArena.download