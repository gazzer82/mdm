#!/bin/sh
# Turn on right-click for mouse and trackpad
for USER_TEMPLATE in "/Library/User Template"/*
    do
    if [ ! "${USER_TEMPLATE}" = "/Library/User Template/__permissions.plist" ] 
    then
    /usr/bin/defaults write "${USER_TEMPLATE}"/Library/Preferences/com.apple.AppleHIDMouse.plist Button2 -int 2
    /usr/bin/defaults write "${USER_TEMPLATE}"/Library/Preferences/com.apple.AppleMultitouchMouse.plist MouseButtonMode -string TwoButton
    /usr/bin/defaults write "${USER_TEMPLATE}"/Library/Preferences/com.apple.AppleMultitouchTrackpad.plist TrackpadRightClick -int 1
    /usr/bin/defaults write "${USER_TEMPLATE}"/Library/Preferences/com.apple.driver.AppleBluetoothMultitouch.trackpad.plist TrackpadRightClick -int 1
    /usr/bin/defaults write "${USER_TEMPLATE}"/Library/Preferences/com.apple.driver.AppleBluetoothMultitouch.trackpad.plist Clicking -bool true
    fi
    done


for USER_HOME in /Users/*
    do
        USER_UID=`basename "${USER_HOME}"`
        if [ ! "${USER_UID}" = "Shared" ] 
        then 
            if [ ! -d "${USER_HOME}"/Library/Preferences ]
            then
            mkdir -p "${USER_HOME}"/Library/Preferences
            chown "${USER_UID}" "${USER_HOME}"/Library
            chown "${USER_UID}" "${USER_HOME}"/Library/Preferences
            fi
            if [ -d "${USER_HOME}"/Library/Preferences ]
            then
                killall -u $USER_UID cfprefsd
                /usr/bin/defaults write "${USER_HOME}"/Library/Preferences/com.apple.AppleHIDMouse.plist Button2 -int 2
                /usr/bin/defaults write "${USER_HOME}"/Library/Preferences/com.apple.AppleMultitouchMouse.plist MouseButtonMode -string TwoButton
                /usr/bin/defaults write "${USER_HOME}"/Library/Preferences/com.apple.AppleMultitouchTrackpad.plist TrackpadRightClick -int 1
		        /usr/bin/defaults write "${USER_HOME}"/Library/Preferences/com.apple.AppleMultitouchTrackpad.plist Clicking -bool true
            chown "${USER_UID}" "${USER_HOME}"/Library/Preferences/com.apple.AppleMultitouchMouse.plist
            chown "${USER_UID}" "${USER_HOME}"/Library/Preferences/com.apple.AppleMultitouchTrackpad.plist
            chown "${USER_UID}" "${USER_HOME}"/Library/Preferences/com.apple.AppleHIDMouse.plist
            chown "${USER_UID}" "${USER_HOME}"/Library/Preferences/com.apple.driver.AppleBluetoothMultitouch.trackpad.plist
            fi
        fi
done
